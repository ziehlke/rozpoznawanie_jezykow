import com.detectlanguage.DetectLanguage;
import com.detectlanguage.Result;
import com.detectlanguage.errors.APIError;

import java.io.*;
import java.util.List;
import java.util.Locale;
import java.util.Properties;


public class Main {
    public static void main(String[] args) throws APIError {

        /*
        Set-up your apiKey
        */

        Properties appProps = new Properties();
        try {
            appProps.load(new FileInputStream("src/main/resources/app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        DetectLanguage.apiKey = appProps.getProperty("apiKey");


        File folder = new File("src/main/resources/artykuly/");
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println("-----------------------------");
                System.out.println(file.getName());

                try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {

                    StringBuilder text = new StringBuilder();
                    String line;

                    while ((line = bufferedReader.readLine()) != null) {
                        text.append(line).append(" ");
                    }

                    List<Result> results = DetectLanguage.detect(text.toString());

                    Result result = results.get(0);

                    System.out.println("Language code: " + result.language);
                    Locale locale = new Locale(result.language);
                    System.out.println("Language: " + locale.getDisplayLanguage());
                    System.out.println("Is reliable: " + result.isReliable);
                    System.out.println("Confidence: " + result.confidence);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}